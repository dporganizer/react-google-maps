# react-google-maps - new tag!!!
> React.js Google Maps integration component

[![Version][npm-image]][npm-url] [![Travis CI][travis-image]][travis-url] [![Quality][codeclimate-image]][codeclimate-url] [![Coverage][codeclimate-coverage-image]][codeclimate-coverage-url] [![Dependencies][gemnasium-image]][gemnasium-url] [![Gitter][gitter-image]][gitter-url]

## DPOrganizer fork info

As DPOrganizer use [OverlappingMarkerSpiderfier](https://github.com/jawj/OverlappingMarkerSpiderfier) to expand clustered icons,
having access to the google maps object is needed. This is not supported by the official `react-google-maps` as of 2018-02-28.
The solution is to fork the official repo and add functionality to do so.

### Branching strategy

As this is a cloned repo, not a fork, manual updates to the repo is needed.
Preferably with several `git remote `:
 - `origin` pointing to this [DPOrganizer repo](https://bitbucket.org/dporganizer/react-google-maps)
 - `github` pointing to the [source repo](https://github.com/tomchentw/react-google-maps)

Whenever you clone the repo locally, do that from this bitbucket repo:

```
git clone git@bitbucket.org:dporganizer/react-google-maps.git
```

Then add the `github` remote

```
git remote add github git@bitbucket.org:dporganizer/react-google-maps.git
```

To do a version update from the github remote:

```
git fetch github
git merge github/master
```


### Changes made

In `src/components/GoogleMap.jsx`:

```js
  getMap() {
    return this.context[MAP]
  }
```

In `src/components/Marker.jsx`:

```js
  getMarker() {
    return this.state[MARKER]
  }
```

Don't forget to change the `lib/components` as well, as they are the code used by callers.


## [Introduction](https://tomchentw.github.io/react-google-maps/#introduction)


## [Installation](https://tomchentw.github.io/react-google-maps/#installation)


## [Usage & Configuration](https://tomchentw.github.io/react-google-maps/#usage--configuration)


## [Changelog][changelog-url]

The changelog is automatically generated via [standard-version][standard-version] and can be found in project root as well as npm tarball.


## [Demo App][demo-app-url]

* [Source code][demo-app-source]
* [CodeSandbox](https://codesandbox.io/s/2xyw6n4o9y)

## Getting Help

**Before doing this, did you**:

1. Read the [documentation](https://tomchentw.github.io/react-google-maps)
2. Read the [source code](https://github.com/tomchentw/react-google-maps)


_You can get someone's help in three ways_:

1. Ask on StackOverflow [with a google-maps tag](https://stackoverflow.com/questions/tagged/google-maps?sort=votes&pageSize=50) or [use react-google-maps as a keyword](https://stackoverflow.com/search?q=react-google-maps)
2. Ask in [the chat room][gitter-url]
3. Create a Pull Request with your solutions to your problem

Please, be noted, **no one**, I mean, **no one**, is obligated to help you in **ANY** means. Your time is valuable, so does our contributors. Don't waste our time posting questions like “how do I do X with React-Google-Maps” and “my code doesn't work”. This is not the primary purpose of the issue tracker. Don't abuse.


## For contributors

<details>
  <summary>Some simple guidelines</summary>

* **Don't** manually modify `lib` folder. They're generated during `yarn release` process
* Follow [conventional-commits-specification][conventional-commits-specification]
* [standard-version][standard-version]
* Auto generated: `src/macros` -> `src/components` -> `lib/components`
* Other components are manually maintained
* Use `yarn` and keep `yarn.lock` updated in PR
* Discuss! Discuss! Discuss!

</details>


[npm-image]: https://img.shields.io/npm/v/react-google-maps.svg?style=flat-square
[npm-url]: https://www.npmjs.org/package/react-google-maps

[travis-image]: https://img.shields.io/travis/tomchentw/react-google-maps.svg?style=flat-square
[travis-url]: https://travis-ci.org/tomchentw/react-google-maps
[codeclimate-image]: https://img.shields.io/codeclimate/github/tomchentw/react-google-maps.svg?style=flat-square
[codeclimate-url]: https://codeclimate.com/github/tomchentw/react-google-maps
[codeclimate-coverage-image]: https://img.shields.io/codeclimate/coverage/github/tomchentw/react-google-maps.svg?style=flat-square
[codeclimate-coverage-url]: https://codeclimate.com/github/tomchentw/react-google-maps
[gemnasium-image]: https://img.shields.io/gemnasium/tomchentw/react-google-maps.svg?style=flat-square
[gemnasium-url]: https://gemnasium.com/tomchentw/react-google-maps
[gitter-image]: https://badges.gitter.im/Join%20Chat.svg
[gitter-url]: https://gitter.im/tomchentw/react-google-maps?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge

[changelog-url]: https://github.com/tomchentw/react-google-maps/blob/master/CHANGELOG.md
[demo-app-url]: https://tomchentw.github.io/#/demos/react-google-maps
[demo-app-source]: https://github.com/tomchentw/tomchentw.github.io/blob/master/src/Pages/Demos/ReactGoogleMaps.jsx

[standard-version]: https://github.com/conventional-changelog/standard-version
[conventional-commits-specification]: https://conventionalcommits.org/
